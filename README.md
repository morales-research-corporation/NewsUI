# NewsUI v1.0.1 (final) - this project is now discontinued
The NewsUI is a open-source interface and framework for news apps that would like to have a sleek but vintage newspaper interface!
Warning, the interface is writen in Swift/Playground

This user interface/experience is used in our app called GEO which bring the top news of this week in a brief and simple view currently we have released a stable version of GEO and NewsUI

Includes some bug fixes in the interface and some minor security updates

# This project is now archived as of June 30th, 2023
